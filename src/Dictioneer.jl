#
# Minimalist state manager library intended for syncing program states accross multiple 
# separate processes via sockets/websockets/http/pipes/stdio/tailing-files.


module Dictioneer

include("DictUtils.jl")

import .DictUtils
export DictUtils


import .DictUtils: parsePath,joinPath,offsetPath,deepGet,deepSet,linspace
import Ways: callDomain
import JSON:json


#type shorthand
PathFragment = Union{Int64,String,SubString}

######################################
## running id generators
## (we can generate thousands of ids every second for thousands of years before 64 bit integer overflows)

runningId = 0
function getId()
    global runningId
    runningId+=1
    runningId
end

actionId = 1
function getActionId()
    global actionId
    actionId+=1
    actionId
end

######################################

function test()
    println("hello1")
end


function setSelfDomain(dom)
    defaults[:selfDomain] = dom
end

# Dict(:domain=>dom_,:module=>mod_)
function setHost(host)
    defaults[:host] = host
end



######################################
## structs

# abstract type StatefulOb end


mutable struct Event
    fn::Function
    id::Int64
    path::String
end

mutable struct Host
    domain::String
    mod::String
end


abstract type DictHub

#Container structure for a single nested dictionary
mutable struct DictHub
    data::Dict{<:Any,<:Any} 
    flatdata::Dict{String,<:Any} #Hierarchical subobjects are stored with flat keys. Keys start with "." eg. ".ob.sub.subsub"
                   #Base object is stored in flatdata["."]
    events::Array{Event,1}  #events. Path string access.
    extDomains::Set{String} #connected external domains
    
    id::String
    host::Host
    DictHub(a::Int64) = new() #int argument only to disambiguate from the outer consructor
end
#Container structure for a single nested dictionary
mutable struct FDict
    data::Dict{<:Any,<:Any} 
    flatdata::Dict{String,<:Any} #Hierarchical subobjects are stored with flat keys. Keys start with "." eg. ".ob.sub.subsub"
                   #Base object is stored in flatdata["."]
    events::Array{Event,1}  #events. Path string access.
    extDomains::Set{String} #connected external domains
    
    id::String
    host::Host
    DictHub(a::Int64) = new() #int argument only to disambiguate from the outer consructor
end

mutable struct Hook
    value::Any  #(not really Any)     #Reference to the currently held data. Held for speed and convenience.
                                      #Underlying data object can be altered or swapped to another object in memory.
                                      #Correct object is decided by the path entries
    path::Array{PathFragment}         #Parsed path. Held for efficiency of deep access.
    pathS::String                     #Raw path.    Held for efficiency of flat dict lookups. (e.g. events) 
    hub::DictHub                      #state struct object. Used for triggering events.
    id::Int64                         #var id
    Hook() = new()
end

function DictHub(data,id::String)
    hub = DictHub(1)
    
    hub.data       = data
    hub.events     = []
    #hub.evenIddMap = Dict()
    hub.id         = id
    hub.extDomains = Set()
    
    
    dictHubs[id]   = hub
    hub
end


function getNamedDict(id::String)
    dictHubs[id]
end

##########################################
## internal data holders
##########################################

dictHubs = Dict{String, DictHub}()
actionStore = Dict()
actionDiff = Dict{String, Any}() #stores diffing info for current action
defaults = Dict()

#lock id is global variable rather than hub specific, since multible hubs may change in a single commit
meta = Dict{Symbol,Any}(:lockId=>0)

#store (actionId,channel,domain) tuples
actionQueue = Array{Tuple{Int,Channel,String}}(undef,0)

#Store channels to control action queuing. Key is the actionId (int)
actionRef = Dict{Int,Any}()

##########################################

function getPendingActions()
    return actionStore
end




######################
## Utility functions 
######################

#External domain requests a link to a DictHub.
#<<< duplicate definition
# function initExt( structName::String, sourceDomain::String )
    # hub = dictHubs[structName]
    # push!(hub.extDomains,sourceDomain)
    
    # return hub.data
# end

function deleteHub(hub::DictHub)
    for dom in hub.extDomains
        callDomain(dom,"dictioneer","deleteHub",[hub.id])
    end
    delete!(dictHubs,hub.id)
end
function deleteHub(hubId::String)
    deleteHub(dictHubs[hubId])
end


function bindEvent(hub::DictHub, event::Event)
    events           = get!(hub.events, evemt.path, []) #get, or create, if not present
    events[event.id] = event
end

function unBindEvent(hub::DictHub, event::Event)
    delete!( hub.events[event.path], event.id )
end

function getHubs(starts::String)
    hubs = Dict{String,Any}()
    for (k,v) in dictHubs
        if startswith(k,starts)
            hubs[k] = v
        end
    end
    hubs
end

#outside domain registers a sync event
#The event manager inside the external domain will be given the state hub and a function to call. 
#Julia domain gets only the data path and the calling domain.
function bindExternalSyncEvent(callingDomain::String, DictHubId::String, pathS::String)
    hub  = dictHubs[DictHubId]
    path = parsePath(pathS)
    fun  = ()->callDomain( callingDomain, "dictioneer", "emitSyncEvent", deepGet(hub.data,path) )
    ev   = Event(fun,getId(),pathS)
    bindEvent(hub, ev)
end
function unBindExternalSyncEvent(callingDomain::String, DictHubId::String, pathS::String, eventId::Int64)
    hub  = dictHubs[DictHubId]
    path = parsePath(pathS)
    delete!( hub.events[pathS], eventId )
end

function registerDomain(hub::DictHub, domain::String)
    push!( hub.extDomains, domain )
end
function unRegisterDomain(hub::DictHub, domain::String)
    delete!( hub.extDomains, domain )
end

function emitSyncEvent(hub::DictHub, path::String)
    #get functions registerd to the given path
    events = hub.events[path]
    #call each function in order
    for (id,ev) in events
        ev.fn()
    end
end
function emitSyncEvent(hub::DictHub, path::Array{String})
    emitSyncEvent(hub, join(path,"."))
end


#Make sure all the domains are listening. Non responsive ones are pruned.
#If a domain has only had a temporary snag, it will have to reconnect and do a full refresh.
#<<<we might later add a backlog mechanism that allows temporarily stalled domains to catch up gracefully.
#<<< this is not currently used. Might be fully deprecated.
function emitUpdatePing!(hub::DictHub)
    alive = asyncmap(hub.extDomains) do domName
        ch = callDomain(domName,"dictioneer","ping", [hub.id])
        
        #wait for value or timeout
        while !isready(ch)
            sleep(0.05)
            if time()-t>1
                return (false,domName)
            end
        end
        
        ret = take!(ch) #<<<could add error propagation here. Just ignoring return value now.
        (true,domName) #domain is still alive
    end
    
    
    for (aliv,domName) in alive
        if !aliv
            #Remove the domain that has closed/crashed
            #Events going to this domain will be removed when the event handler attempts to execute.
            delete!(hub.extDomains,domName)
        end
    end
    
    
end


##############
## Diffing ###
##############

#Changes are diffed against the PREVIOUS STATE data. 
#There can be conflicting info if multiple changes are done to the same entry.
#The final diff can consider an entry to be deleted and changed at the same time.
#  This happens if same action first deletes, then sets an entry.
#  The "set" call only sees the original data, where the entry still exists.
#  events need to check "ch" (changed) side first, and ignore "del" (deleted) in this case.
#<<< Note, though. This is still evolving. This might be outdated info.



#Detection of deep changes in shallow paths.
#This allows quick lookups by events.
#Common for all action types
function actionLeadPathDiff(dif::Dict{<:Any,Any},path::Array{PathFragment})
    n = length(path)
    
    for q in 1:n-1
        partial   = path[1:q]
        partialS  = join(partial,".")
        # entryDiff = get!(dif,partialS,Dict{String,Any}("ch"=>Set()))
        
        entryDiff   = get!(dif,partialS,Dict{String,Any}())
        changeQueue = get!(entryDiff,"ch",Set())
        
        push!(changeQueue,path[q+1])
    end
end

function actionDiffSet(dif::Dict{<:Any,Any} ,path::Array{PathFragment} ,hubData::Dict{String,Any})

    leadPath = path[1:end-1]
    leadPathS = join(leadPath,".")
    
    #the entry whose child is being set
    entryDiff = get!(dif,leadPathS,Dict{String,Any}())

    dat = deepGet(hubData,leadPath)
    
    #check whether entry is being changed or added
    if haskey(dat,path[end])
        changes = get!(entryDiff,"ch",Set()) #Set is json formatted as list
        
        #if an entry has been deleted in this action and is now set again,
        #remove the deleted diff.
        if haskey(entryDiff,"del") && ( path[end] in entryDiff["del"] )
            println("revert del")
            if length(entryDiff["del"])>1
                delete!(entryDiff["del"],path[end])
            else
                delete!(entryDiff,"del")
            end
        end
    else
        changes = get!(entryDiff,"add",Set())
    end
    push!(changes,path[end])
    
    #Set each parent level as changed
    actionLeadPathDiff(dif,leadPath)

end

#Only mark the main path as changed
function actionDiffInsert(dif::Dict{<:Any,Any} ,path::Array{PathFragment} ,index)
    pathS     = join(path,".")
    entryDiff = get!(dif,pathS,Dict{String,Any}())
    changes   = get!(entryDiff,"ch",Set())
    push!(changes,index)
    
    leadPath  = path[1:end-1]
    
    #Set each parent level as changed
    actionLeadPathDiff(dif,leadPath)
end

function actionDiffDel(dif::Dict{<:Any,Any} ,path::Array{PathFragment} ,index)
    
    pathS     = join(path,".")
    entryDiff = get!(dif,pathS,Dict{String,Any}())
    changes   = get!(entryDiff,"del",Set())
    
    push!(changes,index)
    
    leadPath  = path[1:end-1]
    
    #Set each parent level as changed
    actionLeadPathDiff(dif,leadPath)
    
end

##############################
## raw hub actions
##############################

#These are mainly called by commitActions(), though they can be called manually too.

function set!(hub::DictHub, path::String, value; ensure=false)
    try 
        pathL = parsePath(path)
        deepSet(hub.data, pathL, value; ensure=ensure, offset=1)
    catch e
        bt  = catch_backtrace()
        msg = sprint(showerror, e, bt)
        println(msg)
    end
end
function Base.delete!(hub::DictHub, path::String, index; ensure=false)
    #get the object and delete the value at the index
    pathL = parsePath(path)
    ob    = deepGet(hub.data, pathL; offset=1)
    
    # +1 is because indices are given 0 based (same with offset above)
    if isa(ob,Array)
        splice!(ob,index+1)
    else
        delete!(ob,index)
    end
end
function Base.insert!(hub::DictHub, path::String, index, value; ensure=false)
    #get the object and insert the value at the index
    pathL = parsePath(path)
    ob = deepGet(hub.data, pathL; offset=1)
    
    # +1 is because indices are given 0 based (same with offset above)
    insert!(ob,index+1,value)
end
function Base.push!(hub::DictHub, path::String, value; ensure=false)
    pathL = parsePath(path)
    ob    = deepGet(hub.data, pathL, ensure=ensure, offset=1)
    push!(ob,value)
end
function Base.append!(hub::DictHub, path::String, value; ensure=false)
    try
        pathL = parsePath(path)
        ob    = deepGet(hub.data, pathL, ensure=ensure, offset=1)
        append!(ob,value)
    catch e
        bt  = catch_backtrace()
        msg = sprint(showerror, e, bt)
        println(msg)
    end
end




#updates external state objects before sync event handlers are invoked.
#Since we generally want identical state on all domains at all times, host domain waits for all to catch up before continuing.
#  Developers must take this into account.
#  <<<this should perhaps have timeout condition for cases where domain has crashed or dropped recently.
function emitUpdateEvent(hub::DictHub, data)
    #call each domain in order
    channels = map(hub.extDomains) do
        callDomain(dom,"dictioneer","handleUpdateEvent", [hub.id,path,data])
    end
    #wait for all channels. (could rather use asyncmap here, if we need to react to channels individually)
    #The tasks run asynchronously, so actual wait time is the longest of the running times, so this can be run naively.
    for ch in channels
        take!(ch) #Blocking
    end
end

##############################
## external calls
##############################

#external domain does an initial clone of the hub data.
function extInit(hubId,extDomain)
    hub  = dictHubs[hubId]
    push!(hub.extDomains,extDomain)
    return hub.data
end


function remoteget(hubId,pathS)
    hub  = dictHubs[hubId]
    data = deepGet(hub,pathS)
end


###################################
## client functions
###################################

#These are used when this domain is in client mode.
#External host sends instructions via these functions.
#<<<It's possible for a domain to be in both modes at once. Must handle per hub basis.











###################################
## state variable
###################################




#<<< currently this only works on hubs that are on the same process?
#    we need to allow communication between several julia domains
#    Flakes apps will run in their own processes separately from the compiler app
#    Also flakes apps might want to communicate with each other
#    
#    If hub is remote, handle it like javascript hooks

#internal use. Sets everything explicitly
# function Hook(hub::DictHub, data, data, path::Array{Union{String,Int}}, pathS::String)
function Hook(hub::DictHub ,value ,path::Array{<:Any} ,pathS::String)
    st = Hook()
    
    st.value  = value    #hold reference for quick access
    st.path   = path
    st.pathS  = pathS
    st.hub    = hub
    st.id     = getId()
    st
end

#create a state from the given state hub
function Hook(hub::DictHub)
    data = hub.data
    Hook(hub, data, [], "")
end

#Initialize state with already known path.
#Users may pass paths as 1-based. Internal calls assume 0-based. (controlled by offset)
function Hook(hub::DictHub, pathS::String; offset=-1)
    path  = parsePath(pathS,offset=offset)
    value  = deepGet(hub.data,path,offset=1)
    
    Hook(hub, value, path, pathS)
end
function Hook(hub::DictHub, path::Array{PathFragment}; offset=-1)
    pathS = joinPath(path)
    value  = deepGet(hub.data,path,offset=1)
    Hook(hub,  value, path, pathS)
end

function Hook(hubId::String, pathS::String=""; offset=-1)
    hub = dictHubs[hubId]
    Hook(hub,pathS,offset=offset) 
end
function Hook(hubId::String, path::Array{PathFragment}; offset=-1)
    hub = dictHubs[hubId]
    Hook(hub,path,offset=offset)
end


#create new variable by concatenating the path of existing state object
#if ensure is true, also creates the given path object if it doesn't exist 
#  <<< ensure is not fully implemented!
function get(var::Hook, pathExt::String; ensure=false)
    frags = parsePath(pathExt,offset=-1) #path extension is given as 1-based. Store it 0 based.
    
    st = Hook()
    
    st.path      = [var.path; frags] #0-based
    
    pathS_ = length(var.pathS)==0 ?  join(frags,".") : join([var.pathS ; frags],".")
    
    st.pathS  = pathS_  #0-based
    st.hub    = var.hub
    st.id     = getId()
    st.value  = deepGet(var.hub.data, st.path,offset=1)
    
    st
end
#convention is that getindex and []-access return a hook. 
#To get the actual value, use eg varname["thing.innerthing"].value
#Doesn't support ensure.
function Base.getindex(var::Hook, index)
    get(var, index)
end

function bindSyncEvent(var::Hook, fn)
    hub   = var.hub
    fun   = ()->fn(var)
    event = Event(fun,getId(),var.pathS)
    bindEvent(hub, event)
end



#Same as sync event, but path is given explicitly and it may differ from the path stored in state variable.
#Usually this is used to trigger event when contents or parents trigger event.
function bindPathEvent(var::Hook, path, fn)
    evt = var.evt
    fun = ()->fn(var)
    bindEvent(evt, path, fun)
end






##############################
## actions
##############################

#List indices in stored paths are 0-based for consistency.
#Paths are still given as 1 based in julia application code.
#(We'll change this if this causes confusion)

#Actions are grouped by hubId. Order of actions only matters inside a hub.

function set!(var::Hook, value; ensure=false)
    
    hubId = var.hub.id
    
    #<<<here we could also store current value into undo buffer
    action = Dict(
         "hubId"=>hubId
        ,"value"=>value
        ,"path"=>var.pathS
        ,"act"=>"set"
    )
    que = get!(actionStore,hubId,Any[])
    push!(que,action)
    
end
function set!(var::Hook, path::String, value; ensure=false)
    
    hubId = var.hub.id
    
    #paths are given 1-based by the user, while stored paths are 0-based.
    pathOff  = offsetPath(Array{PathFragment}(split(path,".")),-1)
    pathOffS = join(pathOff)
    pathS_   = length(var.pathS)==0 ?  path : join([var.pathS,pathOffS],".")
    
    #<<<here we could also store current value into undo buffer
    action = Dict(
         "hubId"=>hubId
        ,"value"=>value
        ,"path"=>pathS_
        ,"act"=>"set"
    )    
    que = get!(actionStore,hubId,Any[])
    push!(que,action)
end

function Base.insert!(var::Hook, index, value; ensure=false)
    
    hubId = var.hub.id
    action = Dict(
         "hubId"=>hubId
        ,"value"=>value
        ,"path"=>var.pathS
        ,"i"=>index
        ,"act"=>"insert"
    )
    que = get!(actionStore,hubId,Any[])
    push!(que,action)
    
end
function Base.insert!(var::Hook, path::String, index, value; ensure=false)
    
    hubId = var.hub.id
    
    pathOff = offsetPath(path,-1)
    pathS_  = length(var.pathS)==0 ?  path : join([var.pathS,pathOff],".")
    
    action = Dict(
         "hubId"=>hubId
        ,"value"=>value
        ,"path"=>pathS_
        ,"i"=>index
        ,"act"=>"insert"
    )
    que = get!(actionStore,hubId,Any[])
    push!(que,action)
end
function Base.delete!(var::Hook, index)
    
    hubId = var.hub.id
    
    action = Dict(
         "hubId"=>hubId
        ,"path"=>var.pathS
        ,"i"=>index
        ,"act"=>"delete"
    )
    
    que = get!(actionStore,hubId,Any[])
    push!(que,action)
    
end
#<<< should this be deprecated? Usually one would fetch the deepest hook and use the no-path version.
function Base.delete!(var::Hook, path::String, index)
    
    hubId = var.hub.id
    
    pathOff = offsetPath(path,-1)
    pathS_  = length(var.pathS)==0 ?  path : join([var.pathS,pathOff],".")
    
    action = Dict(
         "hubId"=>hubId
        ,"path"=>pathS_
        ,"i"=>index
        ,"act"=>"delete"
    )
    
    que = get!(actionStore,hubId,Any[])
    push!(que,action)
end


function Base.append!(var::Hook, value; ensure=false)
    
    hubId = var.hub.id
    # println("appending to ",var.pathS)
    #<<<here we could also store current value into undo buffer
    action = Dict(
         "hubId"=>hubId
        ,"value"=>value
        ,"path"=>var.pathS
        ,"act"=>"append"
    )
    que = get!(actionStore,hubId,Any[])
    push!(que,action)
    
end

#<<<deprecated?
function Base.append!(var::Hook, path::String, value; ensure=false)
    
    hubId = var.hub.id
    
    pathOff = offsetPath(path,-1)
    pathS_  = length(var.pathS)==0 ?  path : join([var.pathS,pathOff],".")
    
    #<<<here we could also store current value into undo buffer
    action = Dict(
         "hubId"=>hubId
        ,"value"=>value
        ,"path"=>pathS_
        ,"act"=>"append"
    )
    que = get!(actionStore,hubId,Any[])
    push!(que,action)
end
function Base.push!(var::Hook, value; ensure=false)
    
    hubId = var.hub.id
    # println("appending to ",var.pathS)
    #<<<here we could also store current value into undo buffer
    action = Dict(
         "hubId"=>hubId
        ,"value"=>value
        ,"path"=>var.pathS
        ,"act"=>"push"
    )
    que = get!(actionStore,hubId,Any[])
    push!(que,action)
    
end

#<<<deprecated?
function Base.push!(var::Hook, path::String, value; ensure=false)
    
    hubId = var.hub.id
    
    pathOff = offsetPath(path,-1)
    pathS_  = length(var.pathS)==0 ?  path : join([var.pathS,pathOff],".")
    
    #<<<here we could also store current value into undo buffer
    action = Dict(
         "hubId"=>hubId
        ,"value"=>value
        ,"path"=>pathS_
        ,"act"=>"push"
    )
    que = get!(actionStore,hubId,Any[])
    push!(que,action)
end

#invoke event without changing anything
function touch(var::Hook)
    
    hubId = var.hub.id
        
    #<<<here we could also store current value into undo buffer
    action = Dict(
         "hubId"=>hubId
        ,"path"=>var.pathS
        ,"act"=>"touch"
    )
    que = get!(actionStore,hubId,Any[])
    push!(que,action)
    
end




###################################
## execution
###################################

#client domain (or local julia) calls. Runs async. 
#Blocks if there's a queue. This causes client call to wait its turn.
function queryAction(fromDomain,mode)
    global meta
    
    actId  = getActionId()
    isBusy = meta[:lockId]!=0
    
    if isBusy
        if mode=="queue"
            ch = Channel(1)
            insert!(actionQueue, 1, (actId,ch,fromDomain))
            take!(ch) #block
        elseif mode=="throttle"
            return Any[actId,true]
        end
    end
    meta[:lockId] = actId #lock this action as active. Other requests will be queued while this is running.
    
    
    return Any[actId,false]
end

function allowNextAction()
    global actionRef,actionQueue
    
    if length(actionQueue)==0
        meta[:lockId] = 0 #unlock all (nothing in queue)
    else
        actId,ch,domain = pop!(actionQueue)
        meta[:lockId] = actId
        put!(ch,true) #unblock the queued action
    end
end
function cancelAction(actionId)
    println("cancelling ",actionId)
    allowNextAction()
    return true
end
function resetActionQueue()
    global meta,actionQueue
    meta[:lockId] = 0
    actionQueue = Array{Tuple{Int,Channel,String}}(undef,0)
    return true
end

function diffAction(hub::DictHub ,actQueue)

    dif = get!(actionDiff,hub.id,Dict{String,Any}())
    
    for act in actQueue
        pathL = Array{PathFragment}(split(act["path"],"."))
        if act["act"] == "set"
            actionDiffSet(dif ,pathL ,hub.data)
        elseif act["act"] == "delete"
            actionDiffDel(dif ,pathL ,act["i"])
        elseif act["act"] == "insert"
            actionDiffInsert(dif ,pathL ,act["i"])
        # elseif act["act"] == "append"
        # elseif act["act"] == "push"
        # elseif act["act"] == "touch"
        end
    end
    dif
end


# actionDiffSet(::Dict{String,Any}, ::String, ::Dict{String,Any})
# actionDiffSet(!Matched::Dict{Any,Any}, !Matched::Array{Union{Int64, String},N} where N, ::Dict{String,Any})

#Acquire lock, call the function (which performs actions), commit.
#If there already is a lock, block until all previous actions complete. If state is incoherent, throw error.
#Usually f is defined in a do block
#Don't call commit() manually when using this.
#<<<actually, to be safe, never call commit() manually.
function action(outCh::Channel, mode::String, f::Function; host::Any=nothing)
    global meta,actionRef,actionQueue

    try
        fromDomain = defaults[:selfDomain] #<<<<<<

        actId,throttled = queryAction(fromDomain,mode) #blocks if host is ready, and not throttling.
        
        if throttled
            return #do nothing in this action
        end
    
        #lock this action
        meta[:lockId] = actId
    
        #run the function
        f(actId)
        
        #commit and unlock
        commit(actId)
        
        put!(outCh,true) #unblock any waiting task. Indicate success.
    catch e
        println("Error during state transition. State might be incoherent.")
        bt = catch_backtrace()
        showerror(stderr, e, bt)
        put!(outCh,false) #unblock any waiting task. Indicate failure.
    end
    
end
#Optionally blocking call.
#Calling take! on returned channel will wait for the action to complete
function action(f::Function, mode::String="queue"; host::Any=nothing)
    ch = Channel(1)
    @async action(ch,mode,f,host=host)
    return ch
end

#When commiting local action
function commit(actId)
    commit(actionStore ,actId ,defaults[:selfDomain])
end
#When commiting external action
#Runs on the host (not on a julia client)
# function commit(extActions::Any=nothing, actId: origin::Any=nothing)
function commit(actions::Any, actId::Any, origin::Any)
    
    try
        
        #Collect transferrable actions for all the affected domains.
        domainQueues = Dict()
        domainDifs   = Dict()
        localDif = Dict()
        
        #For each hub
        for (hubId,queue) in actions
            hub = dictHubs[hubId]
            
            #Perform a diff against current state.
            #This is done per hub.
            dif = diffAction(hub,queue)
            localDif[hubId] = dif
            
            #Ping the domains. The domains should respond immediately.
            #<<<this is probably unnecessary
            #emitUpdatePing!(hub.extDomains)
            
            #Push the actions to outgoing structure.
            #Different domains get different data.
            for dom in hub.extDomains
                #get domain container to which the data is sent
                outQueues = get!(domainQueues,dom,Dict())
                outDifs   = get!(domainDifs,dom,Dict())
                
                #to that domain, assign queue and diff (copies are not made)
                outQueues[hubId] = queue
                outDifs[hubId]   = dif
                
            end
        end

        #Send the update packets to each domain.
        #They will update their local structures.
        #Data contains action queue and dif for each hub separately
        channels = map(collect(keys(domainQueues))) do (dom)
            queues = domainQueues[dom] #get queues for each hub in target domain
            difs   = domainDifs[dom] #get difs for each hub in target domain
            callDomain(dom, "dictioneer", "commitLocalActions", [queues,difs,actId,origin] )
        end
        
        #Commit actions in this domain as well.
        #<<< needs the dif as well. Currently we're not using events in julia, so it's not immediately critical.
        commitLocalActions(actions,actId,origin)
        
        #Wait for all the domains to complete updates before we move on and remove the lock
        #The tasks run asynchronously, so actual wait time is the longest of the running times. This can be run naively.
        for ch in channels
            take!(ch) #Blocking
        end
        
    catch e
        println("Error during state transition. State might be incoherent.")
        bt  = catch_backtrace()
        msg = sprint(showerror, e, bt)
        println(msg)
    end
    
    for (key,val) in actions
        delete!(actions,key)
    end
    
    for (key,val) in actionDiff
        delete!(actionDiff,key)
    end
    
    
    
    # meta[:lockId] = 0
    allowNextAction()
    
    # println("commit end")
end

#might be called locally (if this domain is host) or by other domain (if this this domain is client)
function commitLocalActions(extActions,actId,origin)
    # println("localcommit2 ",extActions)
    #<<<not yet
    #undo = reverse(actions)
    
    
    
    #Update local structures.
    eventPaths = Dict()
    for (hubId,queue) in extActions
    
        #get the relevant hub
        hub = dictHubs[ hubId ]
        
        #insert event list into the collection and get it. 
        events = get!(eventPaths,hub.id,Set(String[]))
        
        #execute each action in order
        for act in queue
            push!(events,act["path"])
        
            # println("act ",act)
            if act["act"] == "set"
                #store relevant event paths
                # println("setting ",meta[:lockId]," ",act)
                set!(hub, act["path"], act["value"])
                
                #push to undo buffer
                #<<<<<
            
            elseif act["act"] == "delete"
                println("deleting ",act)
                delete!(hub, act["path"],act["i"])
            elseif act["act"] == "insert"
                insert!(hub, act["path"],act["i"],act["value"])
            elseif act["act"] == "append"
                append!(hub, act["path"], act["value"])
            elseif act["act"] == "push"
                push!(hub, act["path"], act["value"])
            elseif act["act"] == "touch"
            end
        end
    end
    
    #call local events
    #<<< remember event origin and event id
    
end

#
#<<< unfinished. When this is implemented fully, remember to include event origin <<<<<<<<<
function emitLocalEvents(hub::DictHub ,eventsToRun ,eventId)
    # console.log("hub events",this.events)
    
    # for (let [k,ev] of Object.entries(this.events)){
    for ev in hub.events
        
        if (ev.mode==nothing)
            if !(ev.path in eventsToRun)
                # println("NOPE",ev.path," ",events)
                continue #mode defaults to exact, and no exact match found
            end
            ev.fn(ev)
        elseif(ev.mode=="parent")
            #event also reacts to changes in parent structures
            for evtPath in eventsToRun
                if startswith(ev.path,evtPath)
                    ev.fn(ev)
                    break #only fire once
                end
            end
        elseif(ev.mode=="child")
            #event also reacts to changes in child structures
            for evtPath in eventsToRun
                if startswith(evtPath,ev.path)
                    ev.fn(ev)
                    break #only fire once
                end
            end
        end
    end
end



end
