module Hooks
#

import Ways
W = Ways

import DictUtils:deepSet,deepGet,offsetPath,parsePath


#Hooks is a Surper Simple State Syncinc System.
#It's a simpler version of Dictioneer, and is NOT meant to be used together with Dictioneer (although you can; not recommended).


# hubs = Dict{String,T}() where T <: Any
hubs = Dict{String, Any}()



mutable struct HooksHub
    data::Dict{<:Any,<:Any}
    id::String
    domains::Array{<:Any}
    HooksHub()=new()
end

function HooksHub(hubId,data,domains)
    hub         = HooksHub()
    hub.data    = data
    hub.id      = hubId
    hub.domains = domains
    hubs[hubId] = hub
    hub
end

#Assumes data is set directly to root object, and syncing hooks is not needed.
#Mainly intended for root data initialization before domains connect.
function setObject(self::HooksHub, name,ob)
    self.data[name] = ob
end

function set(self::HooksHub, path::String, data, originId=nothing; offset=0)
    set(self, parsePath(path), data, originId, offset=offset)
end

#assumed to be always deepsetting
function set(self::HooksHub, path, data, originId=nothing; offset=0)
    deepSet(self.data, path, data, offset=offset) #set the data locally
    
    for d in self.domains
        if haskey(W.domains,d)
            pathExt = offsetPath(path,-1) #julia paths are 1 based. Offset the indices for ext domains.
            if typeof(pathExt)!=String
                pathExt = join(pathExt,".")
            end
            W.callDomain(d,"hooks","hooksEvent",[self.id,"set",originId,pathExt,data])
        end
    end
end

function insert(self::HooksHub, path::String, id, value, originId=nothing; offset=0)
    insert(self, parsePath(path), id, value, originId, offset=offset)
end
#Deepget the path
#Insert the value at the given id
#Fire up Events for the path (not path+id)
function insert(self::HooksHub, path, id, value, originId=nothing; offset=0)
    data = deepGet(HooksHub.data,path)
    
    insert!(data,id,value)
    
    for d in self.domains
        if haskey(W.domains,d)
            pathExt = offsetPath(path,-1) #julia paths are 1 based. Offset the indices for ext domains.
            if typeof(pathExt)!=String
                pathExt = join(pathExt,".")
            end
            W.callDomain(d,"hooks","hooksEvent",[self.id,"insert",originId,pathExt,data,id-1])
        end
    end
end

function delete(self::HooksHub, path::String, originId=nothing; offset=0)
    delete(self, parsePath(path),  originId, offset=offset)
end
#Deepget the path
#delete the value at the given path
#Fire up Events for the path
function delete(self::HooksHub, path, originId=nothing; offset=0)
    
    # pathA = path.split(".")
    init  = path[1:end-1]
    toDel = path[end]
    dat   = deepGet(self.data,init)

    println("attempting to delete ",init," ",toDel)
    
    
    if isa(dat,Array)
        splice!(dat,toDel)
    else
        delete!(dat,toDel)
    end
    
    
    for d in self.domains
        if haskey(W.domains,d)
            pathExt = offsetPath(path,-1) #julia paths are 1 based. Offset the indices for ext domains.
            pathOut = join(pathExt[1:end-1],".")
            W.callDomain(d,"hooks","hooksEvent",[self.id,"delete",originId,pathOut,nothing,pathExt[end]])
        end
    end
end

function get(self::HooksHub, path; offset=0,ensure=false,default=nothing)
    if ensure
        return deepGet(self.data, path, offset=offset, ensure=ensure,default=default)
    else
        return deepGet(self.data,path, offset=offset)
    end
end


#

function extGet(hubId::String,path)
    hub = hubs[hubId]
    dat = get(hub,path,offset=1)
    return dat
end

function extSet(hubId::String,path,data,originId)
    # if isa(path,Array)
        # path = Array{Union{Int64, String}}(path)
    # end
    
    pathExt = offsetPath(path,1)
    # @info "setting" pathExt
    hub = hubs[hubId]
    set(hub,pathExt,data,originId)
end
function extInsert(hubId::String,path,id,value,originId)
    #this has the same effect as offsetPath, but doesn't convert back to string
    pathA = parsePath(path,offset=1) 

    hub = hubs[hubId]
    insert(hub,pathA,id,value,originId)
end



#This doesn't emit hooksEvent on ext objects. 
#Maybe it's not an issue, since this should only be called in relatively isolated data.
#  I.e. don't call this on a hook that has mirrors elsewhere.
#<<< This is an issue, if multiple domains use the same root data at the time.
#    Only the caller domain gets updated. 
#    Perhaps change this to catch key error, and call set() on the path.
#    Fix this later.
function extEnsure(hubId::String,path,value)
    hub = hubs[hubId]
    dat = get(hub,path,offset=1,ensure=true,default=value)
    
    return dat
end

#get the data object and delete the given entry
function extDel(hubId::String,path,originId)
    # println("attempting to delete ",path," ",pathExt)
    # hub     = hubs[hubId]
    # pathL   = parsePath(path,offset=1)
    # pathExL = parsePath(pathExt,offset=1)
    
    # println("parsed ",pathL," ",pathExL)
    
    # dat     = get(hub,pathL)
    
    # if isa(dat,Array)
        # splice!(dat,pathExL[1])
    # else
        # delete!(dat,pathExL[1])
    # end
    #<<< add update
    # return dat
    
    
    path2 = parsePath(path,offset=1)
    # @info "setting" pathExt
    hub = hubs[hubId]
    delete(hub,path2,originId)
    
end







mutable struct HooksData
    hubId::String
    hub::HooksHub
    path::Array{Union{String,Int64}}
    value::Any
    #id
    #bound
    HooksData() = new()
end

function HooksData(hubId::String, path)
    hook       = HooksData()
    hook.hubId = hubId
    hook.hub   = hubs[hubId]
    hook.value = deepGet(hook.hub.data, path)
    if typeof(path)==String
        hook.path = parsePath(path)
    else
        hook.path = path
    end
    hook
end

function HooksData(source::HooksData, pathExt)
    lpath      = joinPath(source.path, pathExt)
    
    hook       = HooksData()
    hook.hubId = source.hubId
    hook.hub   = source.hub
    hook.value = deepGet(hook.hub.data, lpath)
    hook.path  = lpath
    hook
end

# <<<inconsistency. disabling for now
#shorthand for get(...).value
# function lget(self::HooksData, pathExt::String)
    # deepGet(self.value,pathExt)
# end

function get(self::HooksData, pathExt::String)
    return HooksData(self, pathExt) #<<<<<<<<<< pathExt is only the extension. this doesn't work
end

function set(self::HooksData, pathExt::String, value)
    self.value = value
    set(self.hub, joinPath(self.path,pathExt),value)
end

function set(self::HooksData, value)
    self.value = value
    set(self.hub, self.path, value)
end

function joinPath(path1, path2S)
    # if path2S==""
        # return path1
    path2 = parsePath(path2S)
    return vcat(path1, path2)
end



end