#
# Minimalist state manager library inted for syncing program states accross multiple 
# separate processes via sockets/websockets/http/pipes/stdio/tailing-files.



############################
# glossary
# sync event: 
#    event and associated functions, which run whenever state data is modified with set(). 
#    This happens right after update request has modified the state in all domains.
#    There can be multiple multiple associated functions per event per domain
# update event: 
#    Event and an associated def for each domain, which run whenever state data is modified with set().
#    This happens in the set() handler before sync event is called.
#    There is only one def call per domain for each set() call.

####################

module Dictioneer



import Main.BasicUtils: parsePath,joinPath,deepGet,deepSet,linspace
import Main.Domains: callDomain
import JSON:json






runningId = 0
def idGen():
    global runningId
    runningId+=1
    runningId





######################################
## structs

abstract type StatefulOb 


mutable struct Event
    fn::Function
    id::Int64
    path::String



class StateStruct():
    dataRoot::Dict{<:Any,<:Any}                 #root state. Used in cases where the invoking object doesn't know the root object
                                            #<<<this might change. It's not known whether each root state has their own event manager.
    events::Dict{String,Dict{Int64,Event}}  #events. Path string access.
    extDomains::Set{String}                 #connected external domains
    id::String
    StateStruct(a::Int64) = new()          #int argument only to disambiguate from the outer consructor


class StateVar():
    dataRoot::Union{Array{<:Any},Dict{<:String,<:Any}}  #root of the data object. Object reference in memory never changes, and is shared with all the state objects.
    data::Any  #(not really Any)                  #Reference to the currently held data. Held for speed and convenience.
                                                   #Underlying data object can be altered or swapped to another object in memory.
                                                   #Correct object is decided by the path entries
    # path::Array{Union{String,Int}}                #Parsed path. Held for efficiency of deep access.
    path::Array{<:Any}                #Parsed path. Held for efficiency of deep access.
    pathS::String                                 #Raw path.    Held for efficiency of flat dict lookups. (e.g. events)
    sts::StateStruct                              #state struct object. Used for triggering events.
    id::Int64                                     #state object id
    StateVar() = new()


def StateStruct(dataRoot,id::String):
    state = StateStruct(1)
    
    state.dataRoot   = dataRoot
    state.events     = Dict()
    #state.evenIddMap = Dict()
    state.id         = id
    state.extDomains = Set()
    state


def initAs!(id::String,dataRoot):
    sts = StateStruct(dataRoot,id)
    stateStructs[id] = sts
    return sts


def initStateAs!(id::String,dataRoot):
    sts = StateStruct(dataRoot,id)
    stateStructs[id] = sts
    return sts


def getNamedState(id::String):
    stateStructs[id]


##########################################
## internal data holders
##########################################

stateStructs = Dict{String, StateStruct}()
actions = Dict(    
)

##########################################

def getPingActions():
    return actions




######################################
## State structure with event manager
######################################

#External domain requests a link to a StateStruct.
def initExt( structName::String, sourceDomain::String ):
    sts = stateStructs[structName]
    push!(sts.extDomains,sourceDomain)
    
    return sts.dataRoot



def bindEvent(sts::StateStruct, event::Event):
    events           = get!(sts.events, evemt.path, []) #get, or create, if not present
    events[event.id] = event


def unBindEvent(sts::StateStruct, event::Event):
    delete!( sts.events[event.path], event.id )


#outside domain registers a sync event
#The event manager inside the external domain will be given the state object and a def to call. 
#Julia domain gets only the data path and the calling domain.
#This def is called by the domaincom call router module
def bindExternalSyncEvent(callingDomain::String, stateStructId::String, pathS::String):
    sts  = StateStructs[stateStructId]
    path = parsePath(pathS)
    fun  = ()->callDomain( callingDomain, "dictioneer", "emitSyncEvent", deepGet(sts.dataRoot,path) )
    ev   = Event(fun,idGen(),pathS)
    bindEvent(sts, ev)

def unBindExternalSyncEvent(callingDomain::String, stateStructId::String, pathS::String, eventId::Int64):
    sts  = StateStructs[stateStructId]
    path = parsePath(pathS)
    delete!( sts.events[pathS], eventId )


def registerDomain(sts::StateStruct, domain::String):
    push!( sts.extDomains, domain )

def unRegisterDomain(sts::StateStruct, domain::String):
    delete!( sts.extDomains, domain )


def emitSyncEvent(sts::StateStruct, path::String):
    #get functions registerd to the given path
    events = sts.events[path]
    #call each def in order
    for (id,ev) in events
        ev.fn()
    

def emitSyncEvent(sts::StateStruct, path::Array{String}):
    emitSyncEvent(sts, join(path,"."))



#Make sure all the domains are listening. Non responsive ones are pruned.
#If a domain has only had a temporary snag, it will have to reconnect and do a full refresh.
#<<<we might later add a backlog mechanism that allows temporarily stalled domains to catch up gracefully.
def emitUpdatePing(sts::StateStruct):
    alive = asyncmap(sts.extDomains) do domName
        ch = callDomain(domName,"dictioneer","ping", [sts.id])
        
        #wait for value or timeout
        while !isready(ch)
            sleep(0.05)
            if time()-t>1
                return (false,domName)
            
        
        
        ret = take!(ch) #<<<could add error propagation here. Just ignoring return value now.
        (true,domName) #domain is still alive
    
    
    
    
    
    
    for (aliv,domName) in alive:
        if !aliv
            #Remove the domain that has closed/crashed
            #Events going to this domain will be removed when the event handler attempts to execute.
            delete!(sts.extDomains,domName)
        
    
    
    







#these are mainly called by commitActions().
def set(sts::StateStruct, path::String, data; ensure=false):
    try 
        pathL = parsePath(path)
        # println("setting ", pathL, " ", data)
        deepSet(sts.dataRoot, pathL, data; ensure=ensure, offset=1)
        
    except e:
        bt  = catch_backtrace()
        msg = sprint(showerror, e, bt)
        println(msg)
    

def app(sts::StateStruct, path::String, data; ensure=false):
    try
        pathL = parsePath(path)
        dat   = deepGet(sts.dataRoot, pathL, ensure=ensure, offset=1)
        push!(dat,data)
    catch e
        bt  = catch_backtrace()
        msg = sprint(showerror, e, bt)
        println(msg)
    




#updates external state objects before sync event handlers are invoked.
#Since we generally want identical state on all domains at all times, host domain waits for all to catch up before continuing.
#  Developers must take this into account.
#  <<<this should perhaps have timeout condition for cases where domain has crashed or dropped recently.
def emitUpdateEvent(sts::StateStruct, data):
    #call each domain in order
    channels = map(sts.extDomains) do
        callDomain(dom,"dictioneer","handleUpdateEvent", [sts.id,path,data])
    
    #wait for all channels. (could rather use asyncmap here, if we need to react to individual channels individually)
    #The tasks run asynchronously, so actual wait time is the longest of the running times, so this can be run naively.
    for ch in channels
        take!(ch) #Blocking! callDomain returns unbuffered channel.
    












###################################
## state variable
###################################


#<<< although stateroot doesn't ever change, it perhaps shouldn't be stored in the statevar.
#    Dataroot should be read via state.sts.dataRoot


#internal use. Sets everything explicitly
# def StateVar(sts::StateStruct, dataRoot, data, path::Array{Union{String,Int}}, pathS::String)
def StateVar(sts::StateStruct, dataRoot, data, path, pathS):
    st = StateVar()
    st.dataRoot  = dataRoot
    st.data      = data
    st.path      = path
    st.pathS     = pathS
    st.sts       = sts
    st.id        = idGen()
    st


#create a state from the given state manager
def StateVar(sts::StateStruct):
    dataRoot = sts.dataRoot
    StateVar(sts, dataRoot, dataRoot, [], "")


#Initialize state with already known path.
#Integer indices in path are assumed to be 0-based
def StateVar(sts::StateStruct, pathS):
    path  = parsePath(pathS)
    data  = deepGet(sts.dataRoot,path,offset=1)
    
    StateVar(sts, sts.dataRoot, data, path, pathS)

def StateVar(sts::StateStruct, path::Array{Union{String,Int}})
    pathS = joinPath(path)
    data  = deepGet(sts.dataRoot,path,offset=1)
    StateVar(sts, sts.dataRoot, data, path, pathS)



#create new state object by concatenating the path of existing state object
#if ensure is true, also creates the given path object if it doesn't exist 
#  <<< ensure is not fully implemented!
def concat(state::StateVar, pathAddition::String, ensure=false):
    frags = parsePath(pathAddition)
    
    st = StateVar()
    
    st.dataRoot  = state.dataRoot
    
    st.path      = [state.path; frags]
    
    pathS_ = length(state.pathS)==0 ?  pathAddition : join([state.pathS,pathAddition],".")
    
    st.pathS     = pathS_
    st.sts       = state.sts
    st.id        = idGen()
    
    st.data      = deepGet(state.dataRoot, st.path,offset=1)
    
    st



def bindSyncEvent(state::StateVar, fn):
    sts   = state.sts
    fun   = ()->fn(state)
    event = Event(fun,idGen(),state.pathS)
    bindEvent(sts, event)




#Same as sync event, but path is given explicitly and it may differ from the path stored in state variable.
#Usually this is used to trigger event when contents or parents trigger event.
def bindPathEvent(state::StateVar, path, fn):
    evt = state.evt
    fun = ()->fn(state)
    bindEvent(evt, path, fun)







#Replaces the current data object directly
def set0!(state::StateVar, data, ensure=false):
    #emit update request to all connected domains
    emitUpdateEvent(state.sts, state.path, data, ensure)
        
    deepSet(state.dataRoot, state.path, data, ensure)
    emitSyncEvent(state.sts, state.pathS)


#set with relative path
def deepSet0!(state::StateVar, path::Array{String}, data, ensure=false):
    
    deepSet(state.data, path, data, ensure)
    
    #emit update request to all connected domains
    
    #emit sync event for the full path of the target object
    path_ = state.pathS * "." * join(path,".")
    emitSyncEvent(state.sts, path_)




def set0!(state::StateVar, path::String, data, ensure=false):
    pathL = parsePath(path)
    set(state, pathL, data, ensure)

def get(state::StateVar):
    dat = deepGet(state.sts.dataRoot, state.path, offset=1)
    state.data = dat #store direct reference for fast access
    return dat

#get with relative path
def get(state::StateVar, path::String, ensure=false):

    path_ = length(state.pathS)==0 ? path : join([state.pathS,path],".")
    
    # println("asdf ",join([state.pathS,path],"."))
    dat = deepGet(state.sts.dataRoot, path_,  ensure=ensure, offset=1)
    state.data = dat #store direct reference for fast access
    return dat

def get(state::StateVar, path::Array{String}, ensure=false):
    dat = deepGet(state.sts.dataRoot, [state.path;path],  ensure=ensure, offset=1)
    state.data = dat #store direct reference for fast access
    return dat






#batched actions:


def set!(state::StateVar, data; ensure=false):
    
    structId = state.sts.id
        
    #<<<here we could also store current data into undo buffer
    action = Dict(
         "structId"=>structId
        ,"data"=>data
        ,"path"=>state.pathS
        ,"act"=>"set"
    )
    que = get!(actions,structId,Any[])
    push!(que,action)
    

def app!(state::StateVar, data; ensure=false):
    
    structId = state.sts.id
    # println("apping to ",state.pathS)
    #<<<here we could also store current data into undo buffer
    action = Dict(
         "structId"=>structId
        ,"data"=>data
        ,"path"=>state.pathS
        ,"act"=>"app"
    )
    que = get!(actions,structId,Any[])
    push!(que,action)
    


def touch(state::StateVar):
    
    structId = state.sts.id
        
    #<<<here we could also store current data into undo buffer
    action = Dict(
         "structId"=>structId
        ,"path"=>state.pathS
        ,"act"=>"touch"
    )
    que = get!(actions,structId,Any[])
    push!(que,action)
    




#set with relative path
def deepSet!(state::StateVar, path::Array{Union{String,Int}}, data; ensure=false):
    pathS = join(path,".")
    deepSet!(state,pathS,data, ensure=ensure)

def deepSet!(state::StateVar, path::String, data; ensure=false):
    
    structId = state.sts.id
    
    pathS_ = length(state.pathS)==0 ?  path : join([state.pathS,path],".")
    
    
    #<<<here we could also store current data into undo buffer
    action = Dict(
         "structId"=>structId
        ,"data"=>data
        ,"path"=>pathS_
        ,"act"=>"set"
    )
    que = get!(actions,structId,Any[])
    push!(que,action)

def deepApp!(state::StateVar, path::String, data; ensure=false):
    
    structId = state.sts.id
    
    pathS_ = length(state.pathS)==0 ?  path : join([state.pathS,path],".")
    
    #<<<here we could also store current data into undo buffer
    action = Dict(
         "structId"=>structId
        ,"data"=>data
        ,"path"=>pathS_
        ,"act"=>"app"
    )
    que = get!(actions,structId,Any[])
    push!(que,action)






def commit0(actions::Array{}; undoable=false):
    #undo = reverse(actions)
    for act in actions
        if act["act"] == "set"
            act[""]
        elseif act["act"] == "app"
            
        
    
    


#<<<<<<<< this is only acting locally now. Add ext domain syncing.
#          <<< you are full of shit. commitActions already does that.
def commit():
    
    commitActions(actions)
    for (key,val) in actions
        delete!(actions,key)
    


#Called by outer domains
def commitActions(extActions):
    #Collect actions to per StateStruct lists.
        #<<< probably changin this so that they are created in that way to begin with.
    #   Note: Order of actions for each struct in a commit shouldn't matter.
    #         Order matters inside a struct (app,insert, etc.)
    #         If the struct order matters, create multiple commits.
    # 
    
    #Collect transferrable actions for all the affected domains.
    #    ping the domains (and silently prune unresponsive domains)
    #    push the per-Struct actions to outgoing per-domain structure.
    #S the update packets to each domain.
    #Wait for confirmation <<<<possibly move to the  of this function.
    #Update local structures.
    
    
    #<<<For each StateStruct:
    #<<<   Push the actions to all domains that share the struct.
    #<<<      this is done first so that local update doesn't delay external updates.
    #<<<      Optionally, the originating domain can be excluded from update, if indicated in the actions dict.
    #<<<         Careful with this, state might go out of sync, if there is an error in the external code.
    #<<<      Optionally actions could be validated before pushing them forward. <<<not sure if needed
    
    #Update local structures.
    #Call local sync events.
    #Wait for active domains to confirm successful update.
    
    
    
    
    #<<<not yet
    #undo = reverse(actions)
    
    #Collect transferrable actions for all the affected domains.
    domains = Dict()
    
    #For each StateStruct
    for (stsId,queue) in extActions:
        sts = stateStructs[stsId]
        
        #Ping the domains. The domains should respond immediately.
        #emitUpdatePing!(sts.extDomains)
        
        #push the actions to outgoing structure.
        for dom in sts.extDomains:
            outDomain = get!(domains,dom,Dict())
            outDomain[stsId] = queue
            
        
    
    
    
    
    #s the update packets to each domain.
    for (dom,dat) in domains:
        
        callDomain(dom, "dict", "commitActions", [dat] )
    
    
    # println("called domains")
    
    #wait for confirmation....
    #<<<< unsure how to handle callDomain returns
    
    
    #Update local structures.
    eventPaths = []
    for (stateId,queue) in extActions:
    
    
        #get the relevant structure
        sts = stateStructs[ stateId ]
        
        
        #execute each action in order
        for act in queue:
            # println("act ",act)
            if act["act"] == "set":
                #store relevant event paths
                eventPaths.append(,act["path"])
                
                set!(sts, act["path"], act["data"])
                
                #push to undo buffer
                #<<<<<
                
            elif act["act"] == "app":
                #<<<<not yet
                eventPaths.append(eventPaths,act["path"])
                #act["act"]
                app!(sts, act["path"], act["data"])
            
            elif act["act"] == "touch":
                eventPaths.append(eventPaths,act["path"])
            
        
    
    
    
    




