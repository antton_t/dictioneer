

module DictUtils

function linspace(start,stop,n)
    collect(range(start,stop, length=n))
end
#

#type shorthand
PathFragment = Union{Int64,String,SubString}

##///////////////////
##// object utils ///
##///////////////////

#offset string path
function offsetPath(path::String, offset::Int64=0)
    join(parsePath(path,offset=offset),".")
end

#offset array path
function offsetPath(path0::Array{PathFragment}, offset::Int64=0)
    path = copy(path0)
    for (i,item) in enumerate(path)
        if typeof(item)==Int 
            path[i]+=offset
        end
    end
    return path
end

function parsePath(path::String; offset::Int64=0)
    
    
    if length(path)==0
        return Array{PathFragment}([])
    end
    
    #flat paths start with ".". They are used as-is as keys.
    if startswith(path,".")
        return PathFragment[path]
    end
    
    pathL = map(split(path, ".")) do item
        maybenum = tryparse(Int64,item)
        if maybenum!=nothing
            return maybenum+offset
        else
            return string(item) #convert from substring
        end
    end
    
    pathL2 = Array{PathFragment}(pathL) #change the type signature

    pathL2
end

function parsePath(path::Array; offset::Int64=0)
    #not sure how to handle this with offsets.
    #maybe just assume parsed paths have the correct offset
    return path
end


#<<< is this actually needed. Just join() seems to suffice.
#    new array syntax needs this.
function joinPath(path::Array{PathFragment})
    if startswith(path[1],".")
        return path[1] #return plain flat path string
    end
    
    pathFrags = map(path) do frag
        if typeof(frag)==Int64
            return "[$frag]"
        else
            return string(frag)
        end
    end
    # pathFrags = map(string,path)
    pathS = join(pathFrags,".")
    pathS
end


function Base.haskey(ar::Array,key::Int)
    size(ar,1) >= key && key > 0
end

#get object entry from deeply nested dict, by using a path to the entry.
function deepGet(
      ob::Union{Array{<:Any},Dict{<:Any,<:Any}}
    , path::Array{PathFragment}
    # , path::Array{T} where T<:Any
    ; offset::Int64=0 #this defaults to 0, but normally julia side get-calls manually set this to 1 on each call. This is more explicit.
    , ensure::Bool=false
    , default::Any=nothing        #if ensure==true, assign this value to the path.
)
    if length(path)==0
        return ob #If path is empty, return the object itself.
    end
    
    res = ob
    n = length(path)
    
    for (i,id) in enumerate(path)
        if(typeof(id)==Int64)
            id+=offset
        end
        if !(haskey(res,id))
            if (ensure)
                if i==n
                    res[id] = default
                else
                    if(typeof(path[i+1])==Int64) #next lookup id is a list index (number)
                        res[id]=[]
                    else
                        res[id]=Dict()
                    end
                end
            else
                println("could not find key: $(id). Available keys: $(keys(res))")
            end
        end
        res = res[id]
    end
    
    res
end

function deepGet(
      ob::Union{Array{<:Any},Dict{<:Any,<:Any}}
    , path::String
    ; offset::Int64=0 #this defaults to 0, but normally julia side get-calls manually set this to 1 on each call. This is more explicit.
    , ensure::Bool=false
    , default::Any=nothing 
)
    if length(path)==0
        return ob #If path is empty, return the object itself.
    end
    pathL = parsePath(path) #, offset = -offset
        
    deepGet(ob,pathL,offset=offset,ensure=ensure,default=default)
    
end
    

#sets value in the nested object. If ensure is set, also creates the nested structure if entries can't be found
function deepSet(
      ob::Dict{<:Any,<:Any} #Union{Array{<:Any},Dict{<:Any,<:Any}}
    , path::Array{PathFragment}
    , value::T where T<:Any
    ; ensure::Bool=false
    , offset::Int64=0 #used in interprocess syncing, when incoming int indices are 0 based
)
    res = ob
    
    n = length(path)
    
    if n==0
        return ob #If path is empty, return the object itself.
    end
    
    #loop up until second last path entry
    #for (var i=0; i<n-1; i++){
    for i in 1:n-1
        id = path[i]
        if(typeof(id)==Int64)
            id+=offset
        end
        if !haskey(res,id)
            if (ensure)
                if(typeof(path[i+1])==Int64) #next lookup id is a list index (number)
                    res[id]=[]
                else
                    res[id]=Dict()
                end
            else
                println("could not find key: $(id) (key number $i; 1-based)")
                println("full path: $path")
            end
        end
        res = res[id]
    end

    #set the value at the last path entry
    if(typeof(path[n])==Int64)
        res[path[n]+offset] = value
    else
        res[path[n]] = value
    end
end

#same as above, but takes a string as path
function deepSet(
      ob::Dict{<:Any,<:Any} 
    , path::String
    , value::T where T<:Any
    ; ensure::Bool=false
    , offset::Int64=0 #used in interprocess syncing, when incoming int indices are 0 based
)
    pathL = parsePath(path)
    deepSet(ob,pathL,value,ensure=ensure,offset=offset)
end

end