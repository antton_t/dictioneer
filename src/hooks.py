import asyncio as A
import datetime as D 
import inspect
from datetime import datetime

import ways as W
import dictutils as U

from dictutils import deepSet,deepGet

#Hooks is a Surper Simple State Syncinc System.
#It's a simpler version of Dictioneer, and is NOT meant to be used together with Dictioneer (although you can; not recommended).

hubs = {}


#Version that doesn't keep hooks active, but instead prompts the domains to fetch data when it becomes available.
#Much simpler, but may have more issues in complex code. With simple guis this is probably preferrable.
class HooksHub():
    def __init__(self, hubId, ob=None, domains=[]):
        self.id      = hubId
        self.data    = ob
        self.hooks   = {}
        self.domains = domains
        self.events  = {}
        hubs[hubId]  = self
        
    #Assumes data is set directly to root object, and syncing hooks is not needed.
    #Mainly intended for root data initialization before domains connect.
    def setObject(self, name,ob):
        self.data[name] = ob
    #assumed to be always deepsetting
    async def set(self,path,data, ensure=False, originId=None):
        deepSet(self.data, path, data, ensure=ensure) #set the data locally

        await self.callBound(path,data) #call bound events
        
        for d in self.domains:
            if d in W.domains:
                #originId is sent back, so that requester doesn't react to its own event.
                await W.callDomain(d,"hooks","hooksEvent",[self.id,"set",originId,path,data,None]) 
    
    def get(self,path,default=None):
        try:
            return deepGet(self.data,path)
        except KeyError as e:
            if default:
                # print(e)
                # print("state",self.data)
                # print(f"hook {path} is defaulting to {default}")
                deepSet(self.data,path,default,ensure=True)
                return default
            else:
                raise
                
    #bind function to a path.
    #When the path is set to, run the function with the path data.
    #eventId is needed to unbind the function later
    #If tree==True, also run the function if any sub path is set to.
    def bind(self,path, eventId, fn, treeReact=False):
        if not path in self.events:
            self.events[path] = {}
        self.events[path][eventId] = {"fn":fn, "tree":treeReact}
        
    async def callBound(self,path,data):
        # print(path,data,self.events)
        for epath,funs in self.events.items():
            # print("all", epath)
            if path.startswith(epath):
                # print("partial",epath,path)
                for eventId,fnDef in funs.items():
                    # print("fn",fnDef)
                    if fnDef["tree"]==False and path!=epath:
                        continue #event is set to react only to exact paths. Skip it on partial matches
                    else:
                        #The functions shouldn't have return values, but if the call
                        #is async, it returns a coroutine, which needs to be awaited.
                        ret = fnDef["fn"](path,data)
                        if inspect.isawaitable(ret):
                            await ret

def extGet(hubId,path):
    hub = hubs[hubId]
    dat = hub.get(path)
    return dat

async def extSet(hubId,path,data,originId=None):
    hub = hubs[hubId]
    await hub.set(path,data,originId)



#This doesn't emit hooksEvent on ext objects. 
#Maybe it's not an issue, since this should only be called in relatively isolated data.
#  I.e. don't call this on a hook that has mirrors elsewhere.
def extEnsure(hubId,path,value):
    hub = hubs[hubId]
    dat = hub.get(path,default=value)
    return dat
    








