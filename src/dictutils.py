
import re

import logging


# selfLogger = logging.getLogger(__name__)
# fileHandler = logging.FileHandler('pythonlog.txt')
# selfLogger.addHandler(fileHandler)
# selfLogger.setLevel(logging.DEBUG)



#general purpose utility functions



# def linspace(start, stop, n):
    # var delta = (stop-start)/(n-1)
    # return range(n).map(function(i){return start+i*delta;})



# def getUniqueId():
    # #get 16 digit random integer within the safe int range
    # var min = 1000000000000000
    # var max = 9000000000000000
    # return Math.floor(Math.random() * (max - min)) + min #The maximum is exclusive and the minimum is inclusive

    

def show(a):
    if(a!=None):
        return JSON.parse(JSON.stringify(a))
    else:
        return None




def toColor(col):
    return f"rgba({col[0]},{col[1]},{col[2]},{col[3]})"



################
## dict utils ##
################

numReg = re.compile(r"[0-9]+")
def parsePath(pathS):
    if len(pathS)==0: return []
    
    pathL = pathS.split(".")
    for i,part in enumerate(pathL):
        #logging.debug(f"adsf {part} {pathS} {pathL}")
        maybeNum = numReg.match(part[0])
        if maybeNum:
            pathL[i] = int(part[0])
    return pathL

#get object entry from deeply nested structure, by using a path to the entry.
def deepGet(ob, path):
    if type(path)==str: 
        path = parsePath(path)
    if len(path)==0: return ob
    res = ob
    for id in path:
        if type(res)==dict:
            if (not (id in res)): raise KeyError(f"could not find key: {id}. Available keys: {res.keys()}")
        else:
            if id>=len(res) or id<0: raise KeyError(f"List id ({id}) out of range. Max {len(res)-1}.")
        res = res[id]
    
    return res



#sets value in the nested object. If ensure is set, also creates the nested structure if entries can't be found
def deepSet(ob, path, value, ensure=False):
    #if root is assigned to, replace all elements, but keep the object reference.
    if len(path)==0:
        if type(ob)==dict:
            keys = list(ob.keys())
            for k in keys: ob.pop(k)
            ob.update(value)
        else:
            while len(ob)>0: ob.pop()
            ob+=value
        return ob
        
    if type(path)==str: 
        path = parsePath(path)
        
    res = ob
    
    n = len(path)
    
    
    for i in range(n):
        id = path[i]
        
        #target object is the second to last entry
        if i==n-1:
            target=res
            
        if (ensure):
            #if res is a list, first ensure that it has available index "id"
            if type(res)==list:
                if id>=len(res):
                    while(len(res)<id+1):
                        res.append(None)
                    if(i<n-1 and type(path[i+1])==int): #next lookup id is a list index (number)
                        res[id]=[]
            else:
                if (not (id in res)):
                    res[id]={}
        else:
            if type(res)==list:
                if id>=len(res) or id<0:  raise KeyError(f"List id ({id}) out of range. Max {len(res)-1}.")
            else:
                if (not (id in res)):
                    raise KeyError(f"could not find key: {id}. Available keys: {res.keys()}")
        res = res[id]
    
    # console.log("setting",path[n-1], 'to', value)
    # console.log(show(res))
    #set the value at the last path entry on the second-to-last object
    target[path[n-1]] = value
    # console.log(show(res))
    


#gets a partial copy of the given object. Effectively drops all elements of the nested object structure that is not mentioned in the lens object.
# EG. 
# ob =  {   item1:1                              lens =  {   item2:{
#           item2:{                                              deepItem2:{
#               deepItem1:2                                          deeper1:true,
#               deepItem2:{                                          deeper3:true,
#                   deeper1:{val:3}                              }}}
#                   deeper2:{veryDeep1:4}
#                   deeper3:5         
#               }}}                               
# will return object structure which has dropped entries  item1, deepItem1 and deeper2 including the subobject
# Deeper1 is returned as is

def lensGet(ob,lens):
    console.log("qq",Object.keys(ob),Object.keys(lens))
    res
    
    if (type(lens)==bool):
        print("collect")
        print("collect",ob)
        return ob
    
    
    if(Array.isArray(ob) and lens[0]=="__each__"): #If desired handle all array elements in the same manner.
                                                   #Otherwise skip this and handle the array as an object (keys are integers).
        if(not (Array.isArray(lens))): raise Exception("Array lens required for an array source. Got: "+type(lens))
        # res = ob.map(item=>lensGet(item,lens[1]))
        res = [lensGet(item,lens[1]) for item in ob]
        return res
    
    
    res = {}
    for key,val in lens.items():
        print(key,val)
        if ( not (key in ob)):
            print("reject",key)
            res[key]=None
        
        else:
            res[key] = lensGet(ob[key],lens[key])
    
    return res


#deep-insert all elements and sub-elements of ob2 into ob1. 
#Missing elements or branches are added.
#Ob1 elements will be replaced by ob2 elements if they conflict.
#NOTE: assumes all elements are primitives, plain objects or arrays. (basically everything JSON serializable is fine)
def objectMerge(ob1,ob2):
    
    
    if(Array.isArray(ob1)): res = []
    else:                   res = {}
    
    #shallow copy ob1. Can't do Object.assign since res can be an array.
    for key in ob1.keys():
        res[key] = ob1[key]
    
    
    for key in ob2.keys():
        if (key in ob1):
            if (type(ob2[key])==dict): #if source data is an object
                if (type(ob1[key])==dict): #if both source and target data are objects
                    branch = objectMerge(ob1[key],ob2[key]) #merge the source and target sub-objects
                    res[key] = branch
                else: #source is an object, target is not. Simple assign. (this should be quite rare; maybe add warning.)
                    res[key] = ob2[key]
            else: #source is not an object. Simple assign. (maybe add warning in case target data is an object, which gets replaced by primitive value)
                res[key] = ob2[key]
        else: #source entry doesn't exists in target. Simple assign
            res[key] = ob2[key]
        
    



#check which keys have been added or removed to or from ob2. Return common elements for convenience.
def diffObjects(ob1, ob2):
    new_     = []
    missing  = []
    common   = []
    
    for (key,val) in ob2.items():
        if (key in ob1):
            common.append(key)
        else:
            new_.append(key)
        
    
    for (key,val) in ob1.items():
        if ( not (key in ob2)):
            missing.append(key)
        
    
    return {
         "new"     : new_   
        ,"missing" : missing
        ,"common"  : common 
    }



#check which items have been added or removed to or from L2. Return common elements for convenience.
#returns the ids instead of values
def diffLists(L1, L2):
    new_     = []
    missing  = []
    common   = []

    for (item,i) in L2:
        if item in L1:
            common.append(i)
        else:
            new_.append(i)
        
    
    for (item, i) in L1:
        if ( not (item in L2)):
            missing.append(i)
        
    
    return {
         "new"     : new_   
        ,"missing" : missing
        ,"common"  : common
    }


def listToDict(L,key):
    ob = {}
    
    for value in L:
        ob[value[key]] = value
    
    return ob






