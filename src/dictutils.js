//this is an ES6 module

//general purpose utility functions

export function maxList(l1,l2) {
    var l3 = []
    l3.length = l1.length
    for(var i=0;i<l1.length;i++){
        l3[i] = Math.max(l1[i],l2[i]);
    }
    return l3;
}
export function zeros(n){
    var v = []
    v.length = n
    for(var i = 0;i<n;i++)
        v[i] = 0;
    return v
}

export function objSize(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
}

export function range(n){
    var a = []
    for(var i=0;i<n;i++){
        a.push(i)
    }
    return a
}

export function linspace(start, stop, n){
    var delta = (stop-start)/(n-1)
    return range(n).map(function(i){return start+i*delta;});
}

export function getRandomInt(min, max) {
    var min = Math.ceil(min);
    var max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

export function getUniqueId() {
    //get 16 digit random integer within the safe int range
    var min = 1000000000000000;
    var max = 9000000000000000;
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}
    

export function show(a){
    if(a!==undefined)
        return JSON.parse(JSON.stringify(a))
    else    
        return undefined
}



export function round(value, decimals) {
    return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}


export function deepCopy(value){
    return JSON.parse(JSON.stringify(value))
}

export function toColor(col){
    return `rgba(${col[0]},${col[1]},${col[2]},${col[3]})`
}


var runningId = 0
export function nextId(){
    runningId+=1
    return runningId
}


export function cyrb53(str, seed = 0) {
    let h1 = 0xdeadbeef ^ seed, h2 = 0x41c6ce57 ^ seed;
    for (let i = 0, ch; i < str.length; i++) {
        ch = str.charCodeAt(i);
        h1 = Math.imul(h1 ^ ch, 2654435761);
        h2 = Math.imul(h2 ^ ch, 1597334677);
    }
    h1 = Math.imul(h1 ^ (h1>>>16), 2246822507) ^ Math.imul(h2 ^ (h2>>>13), 3266489909);
    h2 = Math.imul(h2 ^ (h2>>>16), 2246822507) ^ Math.imul(h1 ^ (h1>>>13), 3266489909);
    return 4294967296 * (2097151 & h2) + (h1>>>0);
};


/////////////////////
//// object utils ///
/////////////////////

//assume path1 is array and path2S is a string 
//(can be undefined or "". Copy of path1 is returned in that case)
export function joinPath(path1, path2S){
    if (path2S===undefined || path2S==="")
        return [].concat(path1)
    let path2 = path2S.split(".")
    return path1.concat(path2)
}
//both paths are strings
export function joinPathS(path1S, path2S){
    let path
    if (path2S===undefined || path2S==="")
        return path1S
    else
        return path1S.concat(".",path2S)
}



//get object entry from deeply nested structure, by using a path to the entry.
export function deepGet(ob, path){
    if (path.length===0) return ob
    if (typeof(path)==="string")
        path = path.split(".")
    
    var res = ob
    for (let id of path){
        if (!(id in res)) throw Error(`could not find key: ${id}. Available keys: ${show(Object.keys(res))}`)
        res = res[id]
    }
    return res
}

function parsePath(path){
    if (typeof(path)==="string"){
        pathL = path.split(".")}
    else{
        pathL = path
    }
    return pathL
}



//sets value in the nested object. If ensure is set, also creates the nested structure if entries can't be found
export function deepSet(ob, path, value, ensure){
    
    if (path.length===0){
        //The entire object is set to be replaced, but in a way that keeps the object reference intact
        //In practice, we remove all existing items and add new ones.
        if (Array.isArray(ob)){
            ob.splice(0,ob.length,...value) //value must be a list as well.
        }else{
            Object.keys(ob).forEach(k=>delete ob[k])
            Object.assign(ob,value) //value must be object as well.
        }
        return
    }
    
    if (typeof(path)==="string")
        path = path.split(".")
    
    
    
    var n = path.length
    
    
    var res = ob
    ensure = ensure===undefined? false : ensure //ensure ensure :)
    
    
    
    //loop up until second last path entry
    for (var i=0; i<n-1; i++){
        let id = path[i]
        if (!(id in res))
            if (ensure)
                if(typeof(path[i+1])==="number") //next lookup id is a list index (number)
                    res[id]=[]
                else
                    res[id]={}
            else  throw `could not find key: ${id} (key number ${i})`
        res = res[id]
    }
    // console.log("setting",path[n-1], 'to', value)
    // console.log(show(res))
    //set the value at the last path entry
    res[path[n-1]] = value
    // console.log(show(res))
    
}

//gets a partial copy of the given object. Effectively drops all elements of the nested object structure that is not mentioned in the lens object.
// EG. 
// ob =  {   item1:1                              lens =  {   item2:{
//           item2:{                                              deepItem2:{
//               deepItem1:2                                          deeper1:true,
//               deepItem2:{                                          deeper3:true,
//                   deeper1:{val:3}                              }}}
//                   deeper2:{veryDeep1:4}
//                   deeper3:5         
//               }}}                               
// will return object structure which has dropped entries  item1, deepItem1 and deeper2 including the subobject
// Deeper1 is returned as is

export function lensGet(ob,lens){
    console.log("qq",Object.keys(ob),Object.keys(lens))
    var res
    
    if (typeof(lens)==="boolean"){
        console.log("collect")
        console.log("collect",ob)
        return ob
    } 
    
    if(Array.isArray(ob) && lens[0]==="__each__"){ //If desired handle all array elements in the same manner.
                                                   //Otherwise skip this and handle the array as an object (keys are integers).
        if(!(Array.isArray(lens))) throw "Array lens required for an array source. Got: "+typeof(lens)
        res = ob.map(item=>lensGet(item,lens[1]))
        return res
    }
    
    res = {}
    Object.entries(lens).forEach(([key,val])=>{
        console.log(key,val)
        if (!(key in ob)){
            console.log("reject",key)
            res[key]=null
        }
        else
            res[key] = lensGet(ob[key],lens[key])
    })
    return res
}

//deep-insert all elements and sub-elements of ob2 into ob1. 
//Missing elements or branches are added.
//Ob1 elements will be replaced by ob2 elements if they conflict.
//NOTE: assumes all elements are primitives, plain objects or arrays. (basically everything JSON serializable is fine)
export function objectMerge(ob1,ob2){
    
    let res
    if(Array.isArray(ob1)) res = []
    else                   res = {}
    
    //shallow copy ob1. Can't do Object.assign since res can be an array.
    Object.keys(ob1).forEach(key=>{
        res[key] = ob1[key]
    })
    
    Object.keys(ob2).forEach(key=>{
        if (key in ob1){
            if (typeof(ob2[key]==="object")) //if source data is an object
                if (typeof(ob1[key]==="object")){ //if both source and target data are objects
                    let branch = objectMerge(ob1[key],ob2[key]) //merge the source and target sub-objects
                    res[key] = branch
                }else //source is an object, target is not. Simple assign. (this should be quite rare; maybe add warning.)
                    res[key] = ob2[key]
            else //source is not an object. Simple assign. (maybe add warning in case target data is an object, which gets replaced by primitive value)
                res[key] = ob2[key]
        }else{ //source entry doesn't exists in target. Simple assign
            res[key] = ob2[key]
        }
    })
}


//check which keys have been added or removed to or from ob2. Return common elements for convenience.
export function diffObjects(ob1, ob2){
    var new_     = []
    var missing  = []
    var common   = []
    
    for (let [key,val] of Object.entries(ob2)){
        if (key in ob1){
            common.push(key)
        }else{
            new_.push(key)
        }
    }
    for (let [key,val] of Object.entries(ob1)){
        if (!(key in ob2)){
            missing.push(key)
        }
    }
    return {
         "new"     : new_   
        ,"missing" : missing
        ,"common"  : common 
    }
}


//check which items have been added or removed to or from L2. Return common elements for convenience.
//returns the ids instead of values
export function diffLists(L1, L2){
    var new_     = []
    var missing  = []
    var common   = []

    for (let [item,i] of L2){
        if (item in L1){
            common.push(i)
        }else{
            new_.push(i)
        }
    }
    for (let [item, i] of L1){
        if (!(item in L2)){
            missing.push(i)
        }
    }
    return {
         "new"     : new_   
        ,"missing" : missing
        ,"common"  : common
    }
}

export function listAsObject(L,key){
    var ob = {}
    
    for(let value of L){
        ob[value[key]] = value
    }
    return ob
}





////////////////////


export function hello(){
    return "hello"
}
export function sayhello(){
    return console.log(hello()+"!!")
    
}


//// some browser utility functions

export function gotoBottom(id){
   var element = document.getElementById(id);
   element.scrollTop = element.scrollHeight - element.clientHeight;
}

//requires jquery
export function hijackErrorStream(){
    var former = console.log;
    console.log = function(msg){
        //former(msg);  //maintains existing logging via the console.
        $("#errlog").append("<div>" + msg + "</div>");

        gotoBottom("errlog");
    }
    window.onerror = function(message, url, linenumber) {
        console.log("JavaScript error: " + message + " on line " + 
                linenumber + " for " + url);
    }
}
