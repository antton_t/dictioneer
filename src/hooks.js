import * as ways from "/ways/ways.js"
import {callDomain} from "/ways/ways.js"
import {deepGet,deepSet,getUniqueId,show} from "/dictioneer/utils.js"

//Only manages events. Does not store data
//HooksData objects connect directly to host
function WaiterHub(){
    this.hooks = {}
    this.datas = {}
    this.host = null
}
WaiterHub.prototype.ensureHub = function(hubId){
    if(!(hubId in this.hooks))
        this.hooks[hubId] = {}
}
WaiterHub.prototype.add = function(hook,hubId){
    let path = hook.path.join(".")
    //console.log("add path!!",path)
    if(!(hubId in this.hooks))
        this.hooks[hubId] = {}
    
    if (!(path in this.hooks[hubId]))
        this.hooks[hubId][path] = new Set()
    
    this.hooks[hubId][path].add(hook)
    // console.log("hoh",this.hooks)
}
WaiterHub.prototype.remove = function(hook){
    let path = hook.path.join["."]
    this.hooks[path].delete(hook) //apparently this is wrong
}
WaiterHub.prototype.event = function(hubId,eventType,originId,path,newValue,opId){
    
    console.log("event",hubId,eventType,originId,path,newValue)
    // console.log("hub events",this.hooks[hubId])
    
    if(eventType==="set"){
        //update the value in the hub structure
        deepSet(this.datas[hubId],path,newValue)
    }else if(eventType==="insert"){
        //insert new value into an array in the hub structure
        let dat = deepGet(this.datas[hubId],path)
        dat.splice(opId,0,newValue)
        console.log("afterinsert",dat)
    }else if(eventType==="delete"){
        // let path = path.split(".")
        // let init  = pathA.slice(0,pathA.length-1)
        // let last  = pathA[pathA.length]
        // let dat  A = deepGet(this.datas[hubId],init)
        let dat   = deepGet(this.datas[hubId],path)
        if (Array.isArray(dat))
            dat.splice(opId,1)
        else
            delete dat[opId]
    }
    
    //update all connected hook values
    if (path in this.hooks[hubId]){
        //all the hooks connected to this path
        let hooks = this.hooks[hubId][path]
        
        //udpate local value on all the hooks
        hooks.forEach((hook)=>{
            hook.value = newValue
        })
        //call all the events once all data has been updated
        // hooks.forEach(hook=>hook.callBound(newValue,originId))
    }
    //Call all the events once all data has been updated.
    //This includes events that react to changes in their subtrees.
    for (let [regPath,hooks] of Object.entries(this.hooks[hubId]) ){ //loop over all registered hook paths on this hub
        
        if (path.startsWith(regPath)){ //partial or full match
            for (let hook of hooks ){  //there may be multiple hooks connected to the same path. Loop over them.
                for (let ev of Object.values(hook.bound) ){ //loop over all bound events on each hook.
                    if(ev.tree===false && regPath!==path) //event is set to react only to exact paths. Skip it on partial matches
                        continue
                    else
                        ev.fn(hook) //call event on either full or partial path match
                }
                
            }
        }
            
        
    }
    
    
}
WaiterHub.prototype.setHost = function(host){
    this.host = host
}
//full sync of the data structure
WaiterHub.prototype.sync = async function(hubId){
    let data = await callDomain(this.host.domain,this.host.module,"extGet",[hubId,""])
    this.datas[hubId] = data
}
//singleton
export var rootHub = new WaiterHub()



export function hooksEvent(hubId,eventType,originId,path,data,opId){
    rootHub.event(hubId,eventType,originId,path,data,opId)
}





//Synchronous constructor.
//This is mainly called by get() calls, where we already know the data is present.
export function HooksData(hubId,path){
    if (typeof(path)==="string")
        this.path = path.length===0 ? [] : path.split(".")
    else
        this.path = path
    
    this.id   = getUniqueId() //id for this hook
    this.bound = {}
    this.hubId = hubId  //id for root data object
    
    
    if(hubId in rootHub.datas)
        this.value = deepGet(rootHub.datas[hubId],this.path)
    rootHub.ensureHub(hubId)
}

// HooksData.prototype.register = function(){
    // this.hub.add(this)
// }
// HooksData.prototype.unRegister = function(){
    // this.hub.remove(this)
// }

//Async constructor
//This ensures the root object has synced, and done that only once.
HooksData.init = async function(hubId,path){
    let hook = new HooksData(hubId,path)
    
    if(!(hook.hubId in rootHub.datas)){
        await rootHub.sync(hubId)
        hook.value = deepGet(rootHub.datas[hubId],hook.path)
    }
    return hook
}
HooksData.prototype.iterate = function(pathExt){
    // console.log("hooksIterate",pathExt)

    let basePath = joinPath(this.path,pathExt)
    let baseData = deepGet(rootHub.datas[this.hubId],basePath)
    
    
    
    var items
    if (typeof(baseData)==="object"){
        return Object.entries(baseData).map(([k,val])=>new HooksData(
            this.hubId,
            basePath.concat(k)
        ))
    }else{
        console.log("itt",typeof(baseData),this.data,baseData)
        return baseData.map((val,i)=>new HooksData(
            this.hubId,
            basePath.concat(i)
        ))
    }
}
//either get new hook or ensure this.value and return self.
//Actual data can be accessed by eg.
//   hook1.get("thing").value to get sub item (if dict) or
//   hook1.get(1).value       to get sub item (if list) or
//   hook1.get().value        to get the current item
//If hook1.value is called alone in a new context, the underlying data may have changed, 
// which may lead to undefined behaviour.
HooksData.prototype.get = function(pathExt){
    // if (pathExt===undefined || pathExt===""){
        // this.value = deepGet(rootHub.datas[this.hubId],this.path)
        // return this
    // }
    return new HooksData(
        this.hubId,
        joinPath(this.path,pathExt),
    )
}

//deprecated: use .get().value
HooksData.prototype.lget = function(pathExt){
    return deepGet(this.data,pathExt)
}
//remote get. Retrieves the data from host
//deprecated?
HooksData.prototype.rget = async function(pathExt){
    
    var path  = joinPath(this.path,pathExt)
    var dat   = await callDomain(rootHub.host.domain,rootHub.host.module,"extGet",[this.hubId,path.join(".")])

    deepSet(rootHub.datas[this.hubId],path,dat)
    return deepGet(rootHub.datas[this.hubId],path) //if full path is "", we must deepget to get the correct root object, so we just use deepGet every time
}
//Gets the value from host. If the value doesn't exist, assign the default value to it.
//Returns a new hook with the ensured value as the data.
//<<<<<<<<<< update to new style
HooksData.prototype.ensure = async function(pathExt,defaultVal){
    var path  = joinPath(this.path,pathExt)
    
    var dat   =  await callDomain(rootHub.host.domain,rootHub.host.module,"extEnsure",[this.hubId,path.join("."),defaultVal])

    //note: extEnsure doesnt update this. Set it locally. Ensure is true.
    deepSet(rootHub.datas[this.hubId], path, dat, true)
    return this.get(pathExt)
}


//local set. Generally, don't call this manually.
//<<<<<<<<
HooksData.prototype.lset = function(pathExt,item){
    var path = joinPath(this.path,pathExt)
    deepSet(this.data,path,item)
}
//remote set. callDomain returns a promise, so this is async. Needs to be awaited
HooksData.prototype.rset = function(pathExt,item){
    let path = joinPath(this.path,pathExt)
    deepSet(rootHub.datas[this.hubId],path,item)
    return callDomain(rootHub.host.domain, rootHub.host.module, "extSet",[this.hubId,path.join("."),item,this.id])
}
HooksData.prototype.set = HooksData.prototype.rset //alias. lset should be the exception, while set is assumed to be remote.

//delete by subpath
//<<<<<<<<<< update to new style
HooksData.prototype.delete = function(pathExt){
    // this.value = deepGet(rootHub.datas[this.hubId],this.path)
    // if (Array.isArray(this.value))
        // this.value.splice(pathExt,1)
    // else
        // delete this.value[pathExt]
    let pathOut = joinPath(this.path,pathExt)
    return callDomain(rootHub.host.domain, rootHub.host.module, "extDel", [this.hubId, pathOut.join("."),this.id])
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<< unfinished
HooksData.prototype.insert = function(pathExt,id,value){
    return callDomain(rootHub.host.domain, rootHub.host.module, "extInsert", [this.hubId, this.path.join("."),pathExt,this.id])
}

HooksData.prototype.bindEvent = function(fn,id,treeReact){
    rootHub.add(this,this.hubId)
    let tree = treeReact===undefined ? false : treeReact
    this.bound[id] = {"fn":fn, "tree":tree}
}
//redundant/deprecated
HooksData.prototype.callBound = function(value,originId){
    //<<<I'm not sure about this. Migth cause problems
    if (originId===this.id) 
        return
    
    // deepSet(this.data,this.path,value)
    // this.value = value
    Object.values(this.bound).forEach(ev=>ev.fn(this))
}

//assume path1 is array and path2S is a string
function joinPath(path1, path2S){
    if (path2S===undefined || path2S==="")
        return [].concat(path1)
    let path2 = path2S.split(".")
    return path1.concat(path2)
}



