//this is an ES6 module
//Intended for browser side state management

import {show,deepSet,deepGet,joinPath,joinPathS} from "./dictutils.js"
import {callDomain,getSelfDomainId,extImport} from "ways"

let remoteget = extImport("host","dictioneer","remoteget")

//tracked externally hosted states
var extStates = {}
var hubs = {} //this is still using the single host convention
var defaults = {}

//cached actions
//grouped by domain and hub (nested in that order)
var actionStore = {}

var meta = {lock:0,lockHost:null}


var eventId = 0
function getEventId(){
    eventId+=1
    return eventId
}

//Host sends action data here. Changes are made and events are run.
//Host always makes the call to this function, even if this domain called the action.
export function commitLocalActions(actionQueues,difs,actId,fromDomain){

    
    //still using the single host convention
    //<<< In all likelihood we'll only ever have single host for clients.
    //    Things get messy and complicated very quickly with multiple hosts.
    //    The host can still act as client to another host.
    let hubs_ = hubs 
    
    //Update local structures. Then call update events.
    
    var updateEvents = {}
    
    console.log("got difs",difs)
    
    // try{
        //actionQueues and difs are grouped per hub
        for (let [hubId,queue] of Object.entries(actionQueues)){
            
            
            //get the relevant structure
            var hub = hubs_[ hubId ]
            
            if(!(hub.id in updateEvents))
                updateEvents[hubId] = new Set() //use set to include all events only once
            
            
            //execute each action in order
            for (let act of queue){
                //store relevant event paths
                updateEvents[hubId].add(act["path"])
                
                //perform updates, if the hub holds data locally
                if (!(hub.remote)){
                    if (act["act"] === "set"){
                        hub.set(act["path"], act["value"])
                    }
                    else if (act["act"] === "delete"){
                        hub.delete(act["path"],act["i"])
                    }
                    else if (act["act"] === "insert"){
                        hub.insert(act["path"],act["i"],act["value"])
                    }
                    else if (act["act"] === "append"){
                        hub.append(act["path"], act["value"])
                    }
                    else if (act["act"] === "push"){
                        hub.push(act["path"], act["value"])
                    }
                    else if (act["act"] === "touch"){
                        
                    }
                }
            }
            
        }
    
        // console.log("evts",updateEvents)
        //<<< For now, we'll just run events right after state update.
        //    This might change if it becomes a problem. Might need to wait for all domains to update.
        //    Also error handling would probably be easier if these were run separately.
        
        
        //ActId is used to ignore update events from the original caller
        //  This is useful e.g. in cases, where caller updates visuals in real time,
        //  and wants to ignore dictioneer events, which might conflict with the real time process.
        //Event id can be used to ignore calls that are unnecessarily made multiple times.
        //Origin has mostly been supplanted by actId. It is kept here just in case.
        let evId = getEventId()
        let evMeta = {"origin":fromDomain,"evId":evId,"actId":actId}
        
        for (let [hubId,events] of Object.entries(updateEvents)){
            hubs_[hubId].emitEvents(events,evMeta,difs)
        }
    // }catch(e){
        // console.trace(e)
    // }
    // console.log("local commit end")
    return 1
}


function ensureActionStore(ob){
    var dom   = ob.host.domain
    var hubId = ob.hub.id
    if (!(dom in actionStore))
        actionStore[dom] = {}
    var acts = actionStore[dom]
    
    if (!(hubId in acts))
        acts[hubId] = []
    
    return acts[hubId]
}

//##############################
//## Action
//##############################


//Calls to action() are executed in the order they are made.
//If there are multiple calls at the same time, each one will wait for previous ones to complete.
//Mode can be one of:
//  queue (default): actions are queued and executed in the order they are called
//  throttle: Only make the call if host is not busy
export async function action(arg1,arg2,arg3){
    //define signatures
    let mode,fn,host
    if (arg2===undefined && arg3===undefined){
        mode = "queue"
        host = defaults.host
        fn = arg1
    }else if(arg3===undefined){
        mode = arg1
        fn   = arg2
        host = defaults.host
    }else{
        mode = arg1
        host = arg2
        fn   = arg3
    }

    //call host to check if another action is active
    let [actionId,busy] = await callDomain(host.domain,host.module,"queryAction",[defaults.selfDomain,mode])

    if (mode==="queue"){
        try{
            await fn(actionId)
            await commit(actionId)
        }catch(e){
            callDomain(host.domain,host.module,"cancelAction",[actionId])
            throw Error(e)
        }
    }else if(mode==="throttle"){
        if (!busy){
            //Only take the action if host's queue is empty
            await fn(actionId)
            await commit(actionId)
        }
    }
}

//deprecated
export async function stateLock(host){
    if (host===undefined)
        host = defaults.host
    meta.lockHost = host
    return callDomain(host.domain,host.module,"stateLock",[])
}

export async function commit(actionId){
    var host = meta.lockHost || defaults.host
    // console.log("sending",actionStore[host.domain])
    
    let acts = actionStore[host.domain]
    delete actionStore[host.domain]
    
    await callDomain(host.domain,host.module,"commit", [acts ,actionId ,defaults.selfDomain])
    
    // Object.keys(actionStore).forEach(k=>delete actionStore[k])
    
    meta.lockHost = null
    meta.lock = 0
}




export function getPendingActions(){
    return actionStore
}

var runningId = 0
function getId(){
    runningId+=1
    return runningId
}

export function handleUpdateEvent(hubId,path,value){
    var hub = extStates[stmId]
    deepSet(hub.data, path, value)
    return 1
}

//sync event from external host
//<<<deprecated
export function emitSyncEvent(stmId,path){
    var hub = extStates[stmId]
    hub.emitSync(path)
    return 1
}



function DictEvent(hook,fn,id,path,mode){
    this.hook = hook
    this.fn   = fn 
    this.id   = id
    this.path = path
    this.mode = mode
}



export function setHost(host){
    defaults.host = host
}
export function getHost(){
    return defaults.host
}
export function setSelfDomain(domain){
    defaults.selfDomain = domain
}
export function getSelfDomain(){
    return defaults.selfDomain
}






//////////////////////////////////////////
// DictHub
//////////////////////////////////////////

export function DictHub(hubId,host,remote){
    
    if (host===undefined){
        this.host = defaults.host
    }else{
        this.host = host
    }
    // console.log("host",this.host)
    //<<< not sure about this
    // var defaults = {
        // undoable: false, //stores reverse operations of all actions
                        // //<<< not implemented yet
    // }
    // options = Object.assign(defaults,options)
    
    this.id          = hubId
    this.events      = []
    
    this.pathMap     = new Map() //<<<
    this.initialized = false
    //is this hub only for coms? No data is stored. All get() calls make a domain call.
    //<<<to be implemented. Also maybe have a separate function to get the data. Get should only get the var.
    //   if hub is set to remote, get-calls will query host. no need for separate function
    this.remote   = remote===undefined ? false : remote 
    
    
    hubs[hubId] = this
}
//does full initial sync of the data structure
//(Class method)
DictHub.init = async function(hubId,host,remote){
    let hub   = new DictHub(hubId,host,remote)
    
    if (remote!==false){
        let resolve
        hub.ensureInit = new Promise((r)=>{resolve=r})
        
        let data  = await callDomain(hub.host.domain,hub.host.module,"extInit",[hubId, defaults.selfDomain])
        
        resolve()
        
        hub.data = data
    }
    hub.initialized = true
    return hub
}
//full sync of the data structure
DictHub.prototype.sync = async function(){
    //<<< do domains still need to self identify?
    let data = await callDomain(this.host.domain,this.host.module,"extGet",[this.hubId,""])
    this.data = data
    this.initialized = true
}
DictHub.prototype.bind = function(evt){
    
    //If event by the same function exists, replace it.
    // for(let [i,ev] of this.events.entries()){
        // if (ev.fn === evt.fn){
            // this.events[i] = evt
            // return
        // }
        
    // }
    //otherwise just push to the event list
	this.events.push(evt)
}
DictHub.prototype.unBind = function(fn){
    
    // for(let [i,ev] of this.events.entries()){
        // if (ev.fn === fn){
            // this.events.splice(i,1)
            // return
        // }
        
    // }
    for(let i = this.events.length-1; i>=0; i--){
        if (this.events[i].fn === fn){
            this.events.splice(i,1)
        }
    }
}

//emit all sync events in "events".
//<<< here we should to add checks to unbind stale events
DictHub.prototype.emitEvents = function(events,evMeta,dif){
    // console.log("hub events",this.events)
    
    // for (let [k,ev] of Object.entries(this.events)){
    for (let ev of this.events){
        ev.meta = evMeta //<<< clunky fix for now
        if (ev.mode===undefined){
            // console.log("evv",ev)
            if(!events.has(ev.path)){
                // console.log("NOPE",ev.path,events)
                continue //mode defaults to exact, and no exact match found
            }
            ev.fn(ev,dif)
        }else if(ev.mode==="parent"){
            //event also reacts to changes in parent structures
            for(let evtPath of events.values()){
                if (ev.path.startsWith(evtPath)){
                    ev.fn(ev,dif)
                    break //only fire once
                }
            }
        }else if(ev.mode==="child"){
            //event also reacts to changes in child structures
            for(let evtPath of events.values()){
                if (evtPath.startsWith(ev.path)){
                    ev.fn(ev,dif)
                    break //only fire once
                }
            }
        }
    }
}

//Events are stored in the hubs, and are deleted along with it.
//Hooks to this hub need to be cleared hy the user.
export function deleteHub(hubId){
    console.log("deleted",hubId)
    delete hubs[hubId]
    return true
}



//data object structure may have been changed by outside process.
//Iterate all direct paths and check object identities against deepGet. Update if needed.
//deepGet objects are always correct.
// DictHub.prototype.rebase = function(){
    
// }

//##############################
//## raw hub actions
//##############################

//directly set local value
DictHub.prototype.set = function(path,value){
    deepSet(this.data, path, value)
}
//directly append values to local array
DictHub.prototype.append = function(path,value){
    var dat = deepGet(this.data, path)
    if (Array.isArray(dat))
        dat.splice(dat.length,0,...value) //append all
    else
        dat.splice(dat.length,0,value) //append one
}
//directly push to local array
DictHub.prototype.push = function(path,value){
    var dat = deepGet(this.data, path)
    dat.push(value)
}
//directly insert into local array
DictHub.prototype.insert = function(path,index,value){
    let dat = deepGet(this.data,path)
    dat.splice(index,0,value)
}
//directly delete local value
DictHub.prototype.delete = function(path,index){
    let dat = deepGet(this.data,path)
    if (Array.isArray(dat))
        dat.splice(index,1)
    else
        delete dat[index]
}



//##############################
//## Hook
//##############################

//hub is the state struct that handles events.
export function Hook(hubId,path,blank,host){
    if(host===undefined) host = defaults.host
    this.host = host
    
    if (path!==undefined){
        if (typeof(path)==="string"){
            this.path   = path.length===0 ? [] : path.split(".")
            this.pathS  = path
        }else{
            this.path  = path
            this.pathS = this.path.join(".")
        }
    }else{
        this.path  = []
        this.pathS = ""
    }
    
    //This is honestly a bit precarious setup. 
    //There could be edge cases where hook is created synchronously before hub has fully initialized.
    //Normally asynchronously initializing hooks should handle this properly in Hook.init().
    if(hubId in hubs && hubs[hubId].initialized){
        
        this.hub   = hubs[hubId]
        
        if (blank===true){
            this.blank = true
            this.value = null
        }else{
            this.blank = false
            
        
            if (this.hub.remote)
                this.value = remoteget(hubId,this.pathS) //remote hooks query host structure directly
            else
                this.value = deepGet(hubs[hubId].data,this.path)
        }
    }
    
    this.hubId = hubId
    this.id    = getId()
    
}
Hook.init = async function(hubId,path,blank,host){
    let hook = new Hook(hubId,path,blank,host)
    let hub
    
    //If the hub doesn't exist, create it.
    //If it exists but hasn't fully initialized, wait for it to initialize before continuing.
    //There are cases, where multiple independent asynchronous inits are called before hub has fully initialized.
    //Only one init should happen, and the rest should just wait.
    if(hubId in hubs){
        hub = hubs[hubId]
        await hub.ensureInit //blocks until the promise resolves
    }else{
        hub = await DictHub.init(hubId,host) //also inserts the hub into the hubs list
    }
    hook.hub   = hub
    
    if (blank===true){
        this.blank = true
        this.value = null
    }else{
        this.blank = false
        
        if (hook.hub.remote)
            hook.value = remoteget(hubId,hook.pathS) //remote hooks query host structure directly
        else
            hook.value = deepGet(hub.data,hook.path)
    }
    return hook
}

//<<<deprecated?
//copy has a different object identity, but is otherwise identical
Hook.prototype.copy = function(){
    return new Hook(this.data, this.path, this.base)
}


//Sets value
Hook.prototype.set = function(pathExt,value){
    //create action and push it to the action queue
    
    let path
    if (pathExt==="")
        path = this.pathS
    else
        path = this.path.concat(pathExt).join(".")

    var hubId = this.hub.id
    var dat = {
         hubId : hubId
        ,value : value
        ,path  : path
        ,act   : "set"
    }
    
    var acts = ensureActionStore(this)
        
    acts.push(dat)
}

Hook.prototype.delete = function(pathExt,index){
    //create action and push it to the action queue
    
    let path
    
    //if called without arguments, delete the current hook value
    if (pathExt===undefined && index===undefined){
        index = this.path.slice(-1)[0]          //"self"
        path  = this.path.slice(0,-1).join(".") //parent path before "self"
    }else{ 
        if (pathExt==="")
            path = this.pathS
        else
            path = this.path.concat(pathExt).join(".") //<<< this works only with single pathExt entry
    }
    var hubId = this.hub.id
    var dat = {
         hubId : hubId
        ,path  : path
        ,i     : index
        ,act   : "delete"
    }
    
    var acts = ensureActionStore(this)
    
    acts.push(dat)
}
Hook.prototype.insert = function(pathExt,index,value){
    //create action and push it to the action queue
    
    let path = joinPathS(this.pathS,pathExt)

    var hubId = this.hub.id
    var dat = {
         hubId : hubId
        ,path  : path
        ,i     : index
        ,value : value
        ,act   : "insert"
    }
    
    var acts = ensureActionStore(this)
        
    acts.push(dat)
}
Hook.prototype.push = function(pathExt,value){
    //create action and push it to the action queue
    
    let path = joinPathS(this.pathS,pathExt)

    var hubId = this.hub.id
    var dat = {
         hubId : hubId
        ,path  : path
        ,value : value
        ,act   : "push"
    }
    
    var acts = ensureActionStore(this)
        
    acts.push(dat)
}

Hook.prototype.append = function(pathExt,value){
    //create action and push it to the action queue
    
    let path = joinPathS(this.pathS,pathExt)

    var hubId = this.hub.id
    var dat = {
         hubId : hubId
        ,path  : path
        ,value : value
        ,act   : "append"
    }
    
    var acts = ensureActionStore(this)
        
    acts.push(dat)
}

Hook.prototype.touch = function(pathExt){
    //create action and push it to the action queue
    
    let path
    if (pathExt===undefined)
        path = this.pathS
    else
        path = this.path.concat(pathExt).join(".")

    var hubId = this.hub.id
    var dat = {
         hubId : hubId
        ,path  : path
        ,act   : "touch"
    }
    
    var acts = ensureActionStore(this)
        
    acts.push(dat)
}




Hook.prototype.get = function(pathEx,blank){
    //can be called without argument to refresh this.value
    if (pathEx===undefined){
        //hook may have started as blank, but now is expected to have value.
        this.blank = false
        if (this.hub.remote)
            this.value = remoteget(this.hub.id,this.pathS)
        else
            this.value = deepGet(this.hub.data,this.path)
        return this
    }
    
    var path = joinPath(this.path,pathEx)
    return new Hook(this.hubId,path,blank,this.host)
}



Hook.prototype.bind = function(fn,mode){
    var hub   = this.hub
    var event = new DictEvent(this,fn,name,this.pathS,mode)
    hub.bind(event)
}
Hook.prototype.unBind = function(fn){
    this.hub.unBind(fn)
}


//Unpacs the Hook
//returns either a list of Hooks,
//or a dict where keys are the original keys, and values are Hooks
Hook.prototype.entries = function(){
    var self = this
    var dat = self.get().value
    
    if (Array.isArray(dat)){
        return dat.map((v,i)=>{
            return self.get(i)
        })
        
    }else if(typeof(dat)==="object" && dat!==null ){ //null is considered object by js
        var d = {}
        Object.entries(dat).forEach(([key,v])=>{
            d[key] = self.get(key)
        })
        return d
    }else{
        return self
    }
        
    
}


